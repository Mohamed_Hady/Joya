from django.shortcuts import render, HttpResponse
import json

# Create your views here.


def index(request):
    context = {}
    return render(request, template_name='website/index.html', context=context)