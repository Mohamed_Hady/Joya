/******** Security ************/
function getCookie(name) {
    var cookieValue = null;
    if (document.cookie && document.cookie !== '') {
        var cookies = document.cookie.split(';');
        for (var i = 0; i < cookies.length; i++) {
            var cookie = jQuery.trim(cookies[i]);
            // Does this cookie string begin with the name we want?
            if (cookie.substring(0, name.length + 1) === (name + '=')) {
                cookieValue = decodeURIComponent(cookie.substring(name.length + 1));
                break;
            }
        }
    }
    return cookieValue;
}

function csrfSafeMethod(method) {
 // these HTTP methods do not require CSRF protection
 return (/^(GET|HEAD|OPTIONS|TRACE)$/.test(method));
}

var csrftoken = getCookie('csrftoken');


/******** Initializations for the page **********/
var page = 1;
var action = 'active';
loadJoyaData(page);
/**************************************
        textbox for user name
************************************/
$(function () {
    // Get the form fields and hidden div
    var checkbox = $("#name-trigger");
    var hidden = $("#hidden-fields");
    hidden.hide();
    checkbox.change(function () {
        // Check to see if the checkbox is checked.
        // If it is, show the fields and populate the input.
        // If not, hide the fields.
        if (checkbox.is(':checked')) {
            // Show the hidden fields.
            hidden.show();
        }
        else {
            hidden.hide();
        }
    });
});
/***********************************************
            Disable BTN OF POST JOYA
************************************************/
$("#type-joya").on('change keyup paste', function () {
    if ($("#type-joya").val() == '') {
        $("#joya-btn").attr("disabled", true);
    }
    else {
        $('.joya-btn').attr('disabled', false);
    }
});
/**************SHOW JOYA at Modal******************/
function showJoyaContent() {
    var joya = $("#type-joya").val();
    $("#joya-popup-text").html(joya);
}
/******************load joya**************/
function loadJoyaData(page) {
    $.ajax({
        url: "/nlp/joya-feed/" + page,
        type: "GET",
        cache: false,
        beforeSend: function(xhr, settings) {
            if(page == 1){
                $("#loader-div").show();   
            }
            if (!csrfSafeMethod(settings.type) && !this.crossDomain) {
                xhr.setRequestHeader("X-CSRFToken", csrftoken);
            }
        },
        success: function (data) {
            $("#loader-div").hide();
            $("#loader-div2").attr("hidden", "");
            function timeSince(date) {
                var joya_time = new Date(date);
                var seconds = Math.floor((new Date() - joya_time) / 1000);
                var interval = Math.floor(seconds / 31536000);
                if (interval > 1) {
                    return interval + " سنة ";
                }
                interval = Math.floor(seconds / 2592000);
                if (interval >= 1) {
                    return interval + " شهر ";
                }
                interval = Math.floor(seconds / 86400);
                if (interval >= 1) {
                    return interval + " يوم ";
                }
                interval = Math.floor(seconds / 3600);
                if (interval >= 1) {
                    return interval + " ساعة ";
                }
                interval = Math.floor(seconds / 60);
                if (interval >= 1) {
                    return interval + " دقيقة ";
                }
                return Math.floor(seconds) + " ثانية ";
            }
            for (var i = 0; i < data.length; i++) {
                var like_icon_color = ''
                if (data[i].user_like == true){
                    like_icon_color = '#fd008d';
                }
                var joya_content = "<div class='newsfeed' id= id_" + i +"> \
                <div class='newsfeed-block'> \
                  <div class='author-container'> \
                    <div class='auth-avatar'> \
                      <img src='static/website/img/russel.jpg' class='img'> \
                    </div> \
                    <div class='auth-name'>\
                      <a target='_blank'>" + data[i].posted_by + "</a> \
                      <p class='time'>منذ " + timeSince(data[i].posted_on) + "</p> \
                    </div> \
                    <p class='content'>" + data[i].story + "</p> \
                    <div class='joya-actions'> \
                      <a style='color: " + like_icon_color + "' class='like' id=like_" + data[i].joya_id + " onclick='likeJoya("+ data[i].joya_id +")'> \
                        <i class='fa fa-heart-o' aria-hidden='true'></i> \
                      </a> \
                      <span class='counter' id=count_" + data[i].joya_id + ">" + data[i].likes + "</span> \
                    </div> \
                  </div> \
                </div> \
              </div>";
                $(".newsfeed-container").append(joya_content);
            }
            action = "active";
        },
        fail: function (jqXhr, textStatus, errorThrown) {
            console.log(errorThrown);
        }
    })
}
/*******************Get J_SCRORE*********************/
function getJScore() {
    $.ajax({
        url: "/nlp/j-score",
        method: "POST",
        data: {
            "story": $('#type-joya').val()
        },
        cache: false,
        beforeSend: function(xhr, settings) {
            if (!csrfSafeMethod(settings.type) && !this.crossDomain) {
                xhr.setRequestHeader("X-CSRFToken", csrftoken);
            }
        },
        success: function (data) {
            localStorage.setItem('joya_id', data.joya_id);
            localStorage.setItem('j_score', data.j_score);
            $("#js_jscoreMsg").html(data.message);
            var desiredLength = 10;
            var score = Math.ceil(data.j_score);
            $(".raised").html(score + "نقاط");
            // Calculate the percentage
            var percent = (score / desiredLength) * 100;
            // Limit the percentage to 100.
            if (percent > 100) {
                percent = 100;
            }
            // Animate the width of the bar based on the percentage.
            $('.progress-bar').animate({
                width: percent + '%'
            }, 500);
            if (score <= 5.0) {
                //disabe publish btn
                $(".post-joya").attr("disabled", "disabled");
            }
        },
        error: function (jqXhr, textStatus, errorThrown) {
            console.log(errorThrown);
        }
    })
}
/******************POST JOYA*******************/
function postJoyaData() {
    // Retrieve the joya_id from storage
    var joya_id = localStorage.getItem('joya_id');
    var j_score = localStorage.getItem('j_score');
    if (j_score > 5) {
        $.ajax({
            url: "/nlp/post-joya",
            type: "POST",
            beforeSend: function(xhr, settings) {
                if (!csrfSafeMethod(settings.type) && !this.crossDomain) {
                    xhr.setRequestHeader("X-CSRFToken", csrftoken);
                }
            },
            data: {
                "joya_id": joya_id,
                "author": $('#hidden_field').val()
            },
            cache: false,
            success: function (data) {
            if(data.success == true){
                $.notify(data.message, 'success');
                setTimeout(function () {
                    window.location.reload(false);
                }, 1000);
            } else
                $.notify(data.message, 'error');
            },
            error: function (jqXhr, textStatus, errorThrown) {
                console.log("error while posting !");
            }
        });
    }
}
/******* GET JOYA DATA ON SCROLL  **/
$(window).scroll(function () {
    if ($(window).scrollTop() + $(window).height() > $('#newsfeed-container-js').height() && action == 'active') {
        action = 'inactive';
        page++;
        setTimeout(function () {
            if (page != 1) {
                $("#loader-div2").removeAttr("hidden");
            }
            loadJoyaData(page);
        }, 1000);
    }
});
/******************FAVORITE JOYA*******************/
 var counter = 0;
function likeJoya(id) { 
    
            $.ajax({
                url: "/nlp/like-joya",
                type: "POST",
                beforeSend: function(xhr, settings) {
                    if (!csrfSafeMethod(settings.type) && !this.crossDomain) {
                        xhr.setRequestHeader("X-CSRFToken", csrftoken);
                    }
                },
                data: {
                    "joya_id": id,
                },
                cache: false,
                success: function (data) {
                    if(data.success == true){
                        $('#like_'+id).css("color", "#fd008d");
                        counter++;
                        $('#count_'+id).html(counter);
                    }else{
                         $.notify(data.message, 'error');
                    } 
                },
                error: function (jqXhr, textStatus, errorThrown) {
                    console.log("error while liking joya !");
                }
            });
}
/****************** Publish Button ******************/
$('#publish-button').click(function () {
    postJoyaData();
});
/****************** Joya Button ******************/
$('#joya-btn').click(function () {
    getJScore();
    showJoyaContent();
});