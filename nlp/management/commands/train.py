from django.core.management.base import BaseCommand

from nlp.management.commands.train_controllers import (
    validate_and_load_data, train_classifiers,
    test_classifiers, log_statistics, save_and_log)


class Command(BaseCommand):
    help = "Train the classification models"

    def add_arguments(self, parser):
        parser.add_argument('training_file', nargs='+', type=str)

    def handle(self, *args, **options):

        [training_set, testing_set, word_features] = \
            validate_and_load_data(options)

        classifiers = train_classifiers(training_set)

        accuracies = test_classifiers(classifiers, testing_set)

        logging_text = log_statistics(training_set, testing_set, accuracies)

        save_and_log(logging_text, classifiers, word_features)

        self.stdout.write(self.style.SUCCESS(logging_text))
