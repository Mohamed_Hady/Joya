import os
import random

from nltk import FreqDist, NaiveBayesClassifier, classify
from nltk.classify.scikitlearn import SklearnClassifier
from nltk.tokenize import word_tokenize
from sklearn.linear_model import LogisticRegression, SGDClassifier
from sklearn.naive_bayes import BernoulliNB, MultinomialNB
from sklearn.svm import LinearSVC, SVC
from django.core.management.base import CommandError

from nlp.data_helper import (
    clean, features, load_data, save_binary, save_text)


def validate_and_load_data(options):
    if(len(options['training_file']) != 1):
        raise CommandError('Enter only one training file')
    elif(not options['training_file'][0].endswith('.csv')):
        raise CommandError('Enter a .csv file')
    elif(not os.path.exists("nlp/data/{}".format(
         options['training_file'][0]))):
        raise CommandError(
            'Enter a correct file name that exists in nlp/data/ directory')

    file_name = options['training_file'][0]

    # load data as pandas data frame
    data_frame = load_data("nlp/data/{}".format(file_name))

    documents = []
    words_freq = []

    for(_, row) in data_frame.iterrows():
        # clean data from punctuations and digits
        row["Story"] = clean(row["Story"])
        # tokenize it
        tokenized_story = word_tokenize(row["Story"])
        # converting it to list of tuples (words, label)
        documents.append((tokenized_story, row["Label"]))
        # getting all words in the data
        words_freq.extend(tokenized_story)

    random.shuffle(documents)

    words_freq = FreqDist(words_freq)
    # get most informative features
    # (will take top 3000 features when the training set become bigger)
    word_features = list(words_freq.keys())

    featuresets = [(features(story, word_features),
                   label) for (story, label) in documents]
    # take 10% of featuresets to test
    size = int(0.1 * len(featuresets))

    training_set = featuresets[size:]
    testing_set = featuresets[:size]

    return [training_set, testing_set, word_features]


def train_classifiers(training_set):
    classifiers = []
    # NaiveBayesClassifier
    classifiers.append((NaiveBayesClassifier.train(training_set),
                        "Naive Bayes"))
    # MultinomialNB
    classifiers.append((SklearnClassifier(MultinomialNB()).train(training_set),
                        "Multinomial Naive Bayes"))
    # BernoulliNB
    classifiers.append((SklearnClassifier(BernoulliNB()).train(training_set),
                        "Bernoulli Naive Bayes"))
    # LogisticRegression
    classifiers.append((
        SklearnClassifier(LogisticRegression()).train(training_set),
        "Logistic Regression"))
    # SGDClassifier
    classifiers.append((SklearnClassifier(SGDClassifier()).train(training_set),
                        "SGD"))
    # SVC
    classifiers.append((SklearnClassifier(SVC()).train(training_set), "SV"))
    # LinearSVC
    classifiers.append((SklearnClassifier(LinearSVC()).train(training_set),
                        "Linear SV"))
    return classifiers


def test_classifiers(classifiers, testing_set):
    accuracies = []
    for (classifier, name) in classifiers:
        accuracies.append((classify.accuracy(classifier, testing_set) * 100,
                          name))
    accuracies.sort(reverse=True)
    return accuracies


def log_statistics(training_set, testing_set, accuracies):
    # Logging data
    data_set = training_set
    data_set.extend(testing_set)

    dataset_size = len(data_set)
    training_set_size = len(training_set)
    testing_set_size = len(testing_set)

    labels_count = {
        "Positive": 0,
        "Neutral": 0,
        "Negative": 0
    }

    for (_, label) in data_set:
        if(label in labels_count):
            labels_count[label] += 1

    avg_accuracy = 0
    for (accuracy, _) in accuracies:
        avg_accuracy += accuracy
    avg_accuracy /= float(len(accuracies))

    logging_text = """
    # Joya classification models statistics
    ## Data
    * Data set size: {} rows
    * Training set size: {} rows ({:.2f}%)
    * Testing set size: {} rows ({:.2f}%)
    * Labels (Positive, Neutral, Negative)
        - Positive size: {} rows ({:.2f}%)
        - Neutral size: {} rows ({:.2f}%)
        - Negative size: {} rows ({:.2f}%)

    ## Classifiers
    * Classifiers accuracies (sorted)\n""".format(
        dataset_size,
        training_set_size,
        (float(training_set_size) / dataset_size) * 100,
        testing_set_size,
        (float(testing_set_size) / dataset_size) * 100,
        labels_count["Positive"],
        (float(labels_count["Positive"]) / dataset_size) * 100,
        labels_count["Neutral"],
        (float(labels_count["Neutral"]) / dataset_size) * 100,
        labels_count["Negative"],
        (float(labels_count["Negative"]) / dataset_size) * 100)
    idx = 1
    for (accuracy, algo) in accuracies:
        logging_text += "        {}. {}: {:.2f}%\n".format(idx, algo, accuracy)
        idx += 1
    logging_text += "    * Average accuracy: {:.2f}%\n".format(avg_accuracy)

    return logging_text


def save_and_log(logging_text, classifiers, word_features):
    cwd = os.getcwd()
    os.chdir("nlp/classifiers")
    dir_list = [dir for dir in os.listdir() if os.path.isdir(dir)]
    folder_name = "version-{}.0".format(len(dir_list) + 1)
    os.makedirs(folder_name)
    os.chdir(folder_name)
    for (classifier, name) in classifiers:
        save_binary(classifier, os.path.join(os.getcwd(),
                    "{}.pickle".format(name.replace(" ", ""))))

    save_binary(word_features, os.path.join(os.getcwd(),
                "wordfeatures.pickle"))

    save_text(logging_text, os.path.join(os.getcwd(), "README.md"))
    os.chdir(cwd)
