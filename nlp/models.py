# -*- coding: utf-8 -*-

from __future__ import unicode_literals

from django.db import models
from django.utils import timezone

# Create your models here.


class Joyer(models.Model):
    name = models.CharField('Name', max_length=20)
    cookie = models.CharField('Cookie', max_length=500, default=None)

    def __unicode__(self):
        return self.name

    class Meta:
        verbose_name = 'Joyer'
        verbose_name_plural = 'Joyers'


class Joya(models.Model):
    story = models.TextField('Joya Story')
    j_score = models.FloatField('J-Score')
    likes = models.IntegerField('Likes', default=0)
    sent_on = models.DateTimeField('Sent On', default=timezone.now)
    sent_by = models.ForeignKey(Joyer, default=None)

    published = models.BooleanField('Published', default=False)
    published_on = models.DateTimeField('Published On', default=None, null=True, blank=True)

    def __unicode__(self):
        return self.story

    class Meta:
        verbose_name = 'Joya'
        verbose_name_plural = 'Joyas'
        ordering = ['-sent_on']


class Like(models.Model):
    joya = models.ForeignKey(Joya)
    joyer = models.ForeignKey(Joyer)

    class Meta:
        unique_together = ("joya", "joyer")