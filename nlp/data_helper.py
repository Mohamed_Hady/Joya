import pickle
import string

import pandas as pd


def load_data(csv_file_path):
    return pd.read_csv(csv_file_path)


def clean(text):
    exclude = string.punctuation
    exclude += string.digits
    text = ''.join([c for c in text if c not in exclude])
    return text


def features(words, word_features):
    words = set(words)
    features = {}
    for word in word_features:
        features[word] = (word in words)
    return features


def save_binary(classifier, path):
    with open(path, "wb") as classifier_f:
        pickle.dump(classifier, classifier_f)


def save_text(text, path):
    with open(path, "w") as file:
        file.write(text)


def load_binary(path):
    with open(path, "rb") as classifier_f:
        classifier = pickle.load(classifier_f)
    return classifier
