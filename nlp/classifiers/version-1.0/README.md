# Joya classification models statistics
## Data
* Data set size: 107 rows
* Training set size: 97 rows (90.65%)
* Testing set size: 10 rows (9.35%)
* Labels (Positive, Neutral, Negative)
    - Positive size: 46 rows (42.99%)
    - Neutral size: 20 rows (18.69%)
    - Negative size: 41 rows (38.32%)

## Classifiers
* Classifiers accuracies (sorted)
    1. SGD: 100.00%
    2. Multinomial Naive Bayes: 90.00%
    3. Logistic Regression: 90.00%
    4. Linear SV: 90.00%
    5. Bernoulli Naive Bayes: 90.00%
    6. SV: 60.00%
    7. Naive Bayes: 60.00%
* Average accuracy: 82.86%
