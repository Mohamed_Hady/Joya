from django.conf.urls import url

from . import views

urlpatterns = [
    url(r'^joya-feed/(?P<page_id>[0-9]+)$', views.joya_feed, name='joya-feed'),
    url(r'^j-score$', views.j_score, name='j-score'),
    url(r'^post-joya$', views.post_joya, name='post-joya'),
    url(r'^like-joya$', views.like_joya, name='like-joya'),
]