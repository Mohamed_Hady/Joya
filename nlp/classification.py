import random
from statistics import mode

from nltk.classify import ClassifierI
from nltk.tokenize import word_tokenize

from nlp.data_helper import features, load_binary


class Classifier():

    def __init__(self, version):
        self.__nb_classifier = load_binary(
            "nlp/classifiers/version-{}/NaiveBayes.pickle".format(version))
        self.__mnb_classifier = load_binary(
            "nlp/classifiers/version-{}/MultinomialNaiveBayes.pickle"
            .format(version))
        self.__bnb_classifier = load_binary(
            "nlp/classifiers/version-{}/BernoulliNaiveBayes.pickle"
            .format(version))
        self.__lr_classifier = load_binary(
            "nlp/classifiers/version-{}/LogisticRegression.pickle"
            .format(version))
        self.__sgd_classifier = load_binary(
            "nlp/classifiers/version-{}/SGD.pickle".format(version))
        self.__sv_classifier = load_binary(
            "nlp/classifiers/version-{}/SV.pickle".format(version))
        self.__lsv_classifier = load_binary(
            "nlp/classifiers/version-{}/LinearSV.pickle".format(version))
        self.__word_features = load_binary(
            "nlp/classifiers/version-{}/wordfeatures.pickle".format(version))

    def classify(self, text):
        # vote for each classifier and select
        voted_classifier = VoteClassifier(self.__nb_classifier,
                                          self.__mnb_classifier,
                                          self.__bnb_classifier,
                                          self.__lr_classifier,
                                          self.__sgd_classifier,
                                          self.__sv_classifier,
                                          self.__lsv_classifier)
        return voted_classifier.classify(
            features(word_tokenize(text), self.__word_features))


class VoteClassifier(ClassifierI):

    def __init__(self, *classifiers):
        self._classifiers = classifiers

    def classify(self, features):
        votes = []
        for classifier in self._classifiers:
            vote = classifier.classify(features)
            votes.append(vote)
        label = mode(votes)
        if(label == "Negative"):
            return random.randint(0, 4)
        elif(label == "Neutral"):
            return 5
        elif(label == "Positive"):
            return random.randint(6, 10)

    def confidence(self, features):
        votes = []
        for classifier in self._classfiers:
            vote = classifier.classify(features)
            votes.append(vote)
        choice_votes = votes.count(mode(votes))
        return float(choice_votes) / len(votes)
