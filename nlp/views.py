# -*- coding: utf-8 -*-

from django.http import HttpResponse
from . import classifier, joya_card
from .models import Joya, Joyer, Like
import json, random, datetime

# Create your views here.


def joya_feed(request, page_id):
    JOYA_CARDS_COUNT = 10
    start = (int(page_id) - 1) * JOYA_CARDS_COUNT
    end = start + JOYA_CARDS_COUNT

    # see if this user already has a fingerprint
    try:
        joyer = Joyer.objects.get(cookie=request.META.get('HTTP_COOKIE', None))
    except:
        joyer = Joyer(name=joya_card.joyer_name(), cookie=request.META.get('HTTP_COOKIE', None))
        joyer.save()

    joyas = Joya.objects.filter(published=True)[start:end]
    batch = []

    for joya in joyas:
        number_of_likes = Like.objects.filter(joya=joya).count()
        joya.likes = number_of_likes
        joya.save()

        user_like = Like.objects.filter(joya=joya, joyer=joyer).exists()

        card = {'joya_id': joya.id,
                'story': joya.story,
                'posted_by': joya.sent_by.name,
                'posted_on': str(joya.sent_on),
                'likes': joya.likes,
                'j_score': joya.j_score,
                'user_like': user_like,
                'page_id': page_id}
        batch.append(card)
    return HttpResponse(json.dumps(batch), content_type='application/json; charset=utf-8')


def j_score(request):
    story = request.POST.get('story', None)
    if story is None:
        result = {'message': 'Missing Parameters!'}
        return HttpResponse(json.dumps(result), content_type='application/json; charset=utf-8')

    # compute the J-Score
    j_score = classifier.j_score(story)

    # Save this Joya in the database no matter what the score is
    try:
        joyer = Joyer.objects.get(cookie=request.META.get('HTTP_COOKIE', None))
    except:
        joyer = Joyer(name=joya_card.joyer_name(), cookie=request.META.get('HTTP_COOKIE', None))
        joyer.save()

    joya = Joya(story=story,
                j_score=j_score,
                sent_by=joyer)
    joya.save()

    # message
    if joya.j_score <= 5.0:
        message = 'ممكن تخلي حكايتك أكثر سعادة؟'
    else:
        message = 'شكرا لمشاركتك بحكاية سعيدة'

    # send back an appropriate reply
    result = {'joya_id': joya.id,
              'j_score': joya.j_score,
              'message': message}
    return HttpResponse(json.dumps(result), content_type='application/json; charset=utf-8')


def post_joya(request):
    joya_id = request.POST.get('joya_id', None)
    author = request.POST.get('author', None)
    if joya_id is None:
        result = {'message': 'Missing Parameters!'}
        return HttpResponse(json.dumps(result), content_type='application/json; charset=utf-8')

    try:
        joya = Joya.objects.get(pk=joya_id)
    except:
        result = {'message': 'Joya does not exist!'}
        return HttpResponse(json.dumps(result), content_type='application/json; charset=utf-8')

    if author is not None and author != '':
        joyer = Joyer.objects.get(cookie=request.META.get('HTTP_COOKIE', None))
        joyer.name = author
        joyer.save()

    if joya.j_score > 5.0:
        joya.published = True
        joya.published_on = datetime.datetime.now()
        joya.save()
        result = {'success': True,
                  'message': 'تم نشر مشاركتك بنجاح'}
    else:
        result = {'success': False,
                  'message': 'هذه المشاركة تم تصنيفها بسكور أقل من 5'}
    return HttpResponse(json.dumps(result), content_type='application/json; charset=utf-8')

def like_joya(request):
    joya_id = request.POST.get('joya_id', None)
    if joya_id is None:
        result = {'message': 'Missing Parameters!'}
        return HttpResponse(json.dumps(result), content_type='application/json; charset=utf-8')

    try:
        joya = Joya.objects.get(pk=joya_id)
    except:
        result = {'message': 'Joya does not exist!'}
        return HttpResponse(json.dumps(result), content_type='application/json; charset=utf-8')

    try:
        joyer = Joyer.objects.get(cookie=request.META.get('HTTP_COOKIE', None))
    except:
        joyer = Joyer(name=joya_card.joyer_name(), cookie=request.META.get('HTTP_COOKIE', None))
        joyer.save()

    try:
        new_like = Like(joya=joya, joyer=joyer)
        new_like.save()
        result = {'success': True,
                  'message': 'تم مشاركة اعجابك'}
    except:
        result = {'success': False,
                  'message': 'تم مشاركة اعجابك من قبل'}

    return HttpResponse(json.dumps(result), content_type='application/json; charset=utf-8')

