# -*- coding: utf-8 -*-

from django.conf import settings


def j_score(story):

    return settings.CLASSIFIER.classify(story)
