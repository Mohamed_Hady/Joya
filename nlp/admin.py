from django.contrib import admin
from django.contrib.auth.admin import GroupAdmin as BaseGroupAdmin
from django.contrib.auth.models import Group
from .models import Joya, Joyer


@admin.register(Joya)
class JoyaAdmin(admin.ModelAdmin):
    fieldsets = (
        ('Joya', {
            'fields': ('story', 'j_score', 'likes', 'sent_on', 'published', 'published_on',)
        }),
        ('Joyer', {
            'fields': ('sent_by',)
        }),
    )
    list_display = ('story', 'j_score', 'likes', 'sent_on', 'published', 'published_on',)
    # list_editable = ('j_score', )
    # list_filter = ('j_score', 'likes', 'story', )
    list_max_show_all = 50
    list_per_page = 50
    ordering = ('-sent_on', )
    search_fields = ['story']
    show_full_result_count = True
    view_on_site = True


@admin.register(Joyer)
class JoyerAdmin(admin.ModelAdmin):
    fieldsets = (
        ('Joyer', {
            'fields': ('name', 'cookie', )
        }),
    )
    list_display = ('name', 'cookie', )
    list_display_links = ('cookie', )
    list_editable = ('name', )
    # list_filter = ('name', 'cookie', )
    list_max_show_all = 50
    list_per_page = 50
    # ordering = ('name', )
    search_fields = ['name']
    show_full_result_count = True
    view_on_site = True